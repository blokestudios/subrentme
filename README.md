Subrentme

# Subrentme Mobile App

> Subrentme react native source code repository

**Badges will go here**

[![Build Status](http://img.shields.io/travis/badges/badgerbadgerbadger.svg?style=flat-square)](https://travis-ci.org/badges/badgerbadgerbadger) [![Dependency Status](http://img.shields.io/gemnasium/badges/badgerbadgerbadger.svg?style=flat-square)](https://gemnasium.com/badges/badgerbadgerbadger) [![Coverage Status](http://img.shields.io/coveralls/badges/badgerbadgerbadger.svg?style=flat-square)](https://coveralls.io/r/badges/badgerbadgerbadger) [![Code Climate](http://img.shields.io/codeclimate/github/badges/badgerbadgerbadger.svg?style=flat-square)]

---

## Table of Contents (Optional)

- [Installation](#installation)
- [Features](#features)
- [Contributing](#contributing)
- [Team](#team)
- [FAQ](#faq)
- [Support](#support)
- [License](#license)

---

## Installation

- All the `code` required to get started
- Images of what it should look like

### Clone

- Clone this repo to your local machine using `https://bitbucket.org/blokestudios/subrentme`

### Presetup

- You must have xcode and xcode developer tools installed on your machine. You can download xcode from the Mac App Store

- Install Node & Watchman by running in the commands below in a terminal

```shell
$ brew install node
$ brew iinstall watchman
```

- Install CocoaPods

```shell
$ sudo gem install cocoapods
```

### Setup

- Install React Native Dependencies. Navigate to the the root directory of the project ([...]/subrentme) and issue the command below

```shell
$ npm i
```

- Install Pods

```shell
$ cd ios
$ pod install

```

- Open 'subrentme.xcworkspace' file in the ios directory and clean the build folder (shift + cmd+ k), build and run application ( cmd + b)

---

## Features

- Coming Soon

## Usage

- Coming Soon

## Documentation

- Coming Soon

## Tests

- Coming Soon

---

## Contributing

> Coming soon

---

## FAQ

- \*\*Coming soon

---

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
