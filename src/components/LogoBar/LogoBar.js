import React from 'react';
import {Dimensions} from 'react-native';
import styled from 'styled-components';

// eslint-disable-next-line no-unused-vars
const {width} = Dimensions.get('window');

const LogoBar = () => {
  return (
    <Wrap>
      <ImageWrap>{/* <Image source={} resizeMode="contain" /> */}</ImageWrap>
      <BorderBottom />
    </Wrap>
  );
};

const Wrap = styled.View`
  padding-top: 40;
  padding-bottom: 20;
  position: relative;
`;

const ImageWrap = styled.View`
  justify-content: center;
  align-items: center;
  background-color: ${props => props.theme.bgColor};
  height: 40;
`;

const Image = styled.Image`
  height: 20;
  position: relative;
  margin-bottom: 10;
  z-index: 2;
`;

const BorderBottom = styled.View`
  background-color: ${props => props.theme.bgColorLight};
  width: ${width};
  height: 1;
`;

export default LogoBar;
