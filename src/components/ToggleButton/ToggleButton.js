import React, {useState} from 'react';
import {Switch} from 'react-native';
import PropTypes from 'prop-types';

const ToggleButton = props => {
  const {active, disabled, onChange} = props;
  const [isActive, setIsActive] = useState(active);

  const toggle = state => {
    setIsActive(state);
    onChange();
  };

  return <Switch value={isActive} disabled={disabled} onValueChange={toggle} />;
};

ToggleButton.propTypes = {
  active: PropTypes.bool,
  activeColor: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
};

ToggleButton.defaultProps = {
  active: false,
  activeColor: null,
  disabled: false,
};

export default ToggleButton;
