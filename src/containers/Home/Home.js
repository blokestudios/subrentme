import React from 'react';
import styled from 'styled-components';

import LogoBar from '../../components/LogoBar/LogoBar';

const Home = props => {
  return (
    <Wrap>
      <LogoBar />
      <Text>This is the Home component</Text>
    </Wrap>
  );
};

const Wrap = styled.View`
  flex: 1;
  background-color: ${props => props.theme.bgColor};
  padding-top: 4;
  color: ${props => props.theme.textColor};
`;

const Text = styled.Text`
  color: ${props => props.theme.textColor};
`;

export default Home;
