import React, {useEffect, useState} from 'react';
import styled, {ThemeProvider} from 'styled-components';
import {Navigation} from 'react-native-navigation';
import {useSelector, useDispatch} from 'react-redux';
import ToggleButton from '../../components/ToggleButton/ToggleButton';
const packageJSON = require('../../../package.json');
import {setValue} from '../../redux/modules/settings';

const Settings = props => {
  const theme = useSelector(state => state.theme);
  const template = useSelector(state =>
    state.settings.values.find(set => set.key === 'template'),
  ).value;
  const dispatch = useDispatch();
  const {componentId} = props;

  const [uiTheme, setUITheme] = useState(theme);

  const toggleTheme = dark => {
    dispatch(
      setValue({
        key: 'template',
        value: dark ? 'dark' : 'light',
      }),
    );
  };

  useEffect(() => {
    console.log('in effect and output is', theme);
    setUITheme(theme);
    Navigation.mergeOptions(componentId, {
      topBar: {
        title: {
          text: 'Settings',
          color: theme.textColor,
        },
        drawBehind: false,
        visible: true,
        animate: false,
        background: {
          color: theme.bgColor,
        },
        noBorder: true,
      },
      bottomTabs: {
        backgroundColor: theme.bgColor,
      },
      bottomTab: {
        // icon: tabsIcon[template].settings,
        // selectedIcon: tabsIcon[template].selected.settings,
      },
    });
  }, [theme, componentId, template]);

  return (
    <ThemeProvider theme={uiTheme}>
      <Wrap>
        <ToggleButton onChange={toggleTheme} theme={theme} active={false} />
        <Text>This is the settings component</Text>
        <PackageVersion>{`Version ${packageJSON.version}`} </PackageVersion>
      </Wrap>
    </ThemeProvider>
  );
};

const Wrap = styled.View`
  flex: 1;
  background-color: ${props => props.theme.bgColor};
  color: ${props => props.theme.textColor};
  padding-top: 4;
  border-top-width: 1;
  border-bottom-width: 1;
  border-color: ${props => props.theme.bgColorDark};
  border-style: solid;
`;

const Text = styled.Text`
  color: ${props => props.theme.textColor};
  text-align: center;
`;

const PackageVersion = styled.Text`
  color: ${props => props.theme.textColor};
  text-align: center;
  font-size: ${props => props.theme.fontSize.tiny};
  padding-top: 5;
  padding-bottom: 5;
`;

export default Settings;
