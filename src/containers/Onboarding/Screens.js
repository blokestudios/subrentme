import {Navigation} from 'react-native-navigation';

export function startApp() {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: 'sensi.home',
            },
          },
        ],
      },
    },
  });
}
