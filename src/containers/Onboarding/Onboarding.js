import React from 'react';
import styled from 'styled-components';

const Onboarding = props => {
  return (
    <Wrap>
      <Text>This is the onboarding component</Text>
    </Wrap>
  );
};

const Wrap = styled.View`
  flex: 1;
  background-color: ${props => props.theme.bgColor};
  padding-top: 4;
  color: ${props => props.theme.textColor};
`;

const Text = styled.Text`
  color: ${props => props.theme.textColor};
  text-align: center;
`;

export default Onboarding;
