import React from 'react';
import styled from 'styled-components';

import LogoBar from '../../components/LogoBar/LogoBar';

const Store = props => {
  return (
    <Wrap>
      <LogoBar />
      <Text>This is the Lists component</Text>
    </Wrap>
  );
};

const Wrap = styled.View`
  flex: 1;
  background-color: ${props => props.theme.bgColor};
  color: ${props => props.theme.textColor};
  padding-top: 4;
`;

const Text = styled.Text`
  color: ${props => props.theme.textColor};
`;

export default Store;
