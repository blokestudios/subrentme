import React, {Component} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {Navigation} from 'react-native-navigation';
import {Provider} from 'react-redux';

import {registerScreens} from './screens';
import configureStore from './redux/store/configureStore';
import initialState from './redux/store/initialState';
import tabsIcon from './img/tabBarIcons';
import rootSaga from './redux/sagas';
import {getThemeColors} from './theme';

const store = configureStore(initialState);
const state = store.getState();
registerScreens(store, Provider);
store.runSaga(rootSaga);

class App extends Component {
  constructor(props) {
    super(props);
    AsyncStorage.getItem('userToken').then(value => {
      if (value === undefined) {
        this.doInitApp();
      } else {
        this.doInitApp();
      }
    });
  }

  doOnboarding() {
    Navigation.events().registerAppLaunchedListener(() => {
      Navigation.setRoot({
        root: {
          stack: {
            options: {
              topBar: {
                visible: false,
              },
            },
            children: [
              {
                component: {
                  name: 'sbrntme.onboarding',
                },
              },
            ],
          },
        },
      });
    });
  }

  doInitApp() {
    this.setDefaultOptions();
    Navigation.setRoot({
      root: {
        bottomTabs: {
          children: [
            {
              component: {
                name: 'subrentme.home',
                options: {
                  bottomTab: {
                    icon: tabsIcon.home,
                    selectedIcon: tabsIcon.homeSelected,
                  },
                },
              },
            },
            {
              component: {
                name: 'subrentme.list',
                options: {
                  bottomTab: {
                    icon: tabsIcon.store,
                    selectedIcon: tabsIcon.storeSelected,
                  },
                },
              },
            },
            {
              component: {
                name: 'subrentme.activity',
                options: {
                  bottomTab: {
                    icon: tabsIcon.search,
                    selectedIcon: tabsIcon.searchSelected,
                  },
                },
              },
            },
            {
              component: {
                name: 'subrentme.messages',
                options: {
                  bottomTab: {
                    icon: tabsIcon.saved,
                    selectedIcon: tabsIcon.savedSelected,
                  },
                },
              },
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: 'subrentme.settings',
                      options: {
                        bottomTab: {
                          icon: tabsIcon.settings,
                          selectedIcon: tabsIcon.settingsSelected,
                        },
                      },
                    },
                  },
                ],
                options: {},
              },
            },
          ],
        },
      },
    });
  }

  setDefaultOptions() {
    const [theme] = state.settings.values.filter(
      setting => setting.key === 'template',
    );
    const themeColors = getThemeColors(theme.value);
    Navigation.setDefaultOptions({
      bottomTabs: {
        backgroundColor: themeColors.bgColor,
      },
    });
  }

  doHideOnboarding() {
    this.startApp();
  }
}

export default App;
