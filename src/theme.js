export const base = {
  fontSize: {
    tiny: 8,
    small: 12,
    medium: 14,
    large: 18,
    extra: 22,
    mega: 28,
    super: 34,
  },
  thinFontWeight: '100',
  extraLightFontWeight: '200',
  lightFontWeight: '300',
  normalFontWeight: '400',
  mediumFontWeight: '500',
  seimiBoldFontWeight: '600',
  boldFontWeight: '700',
  extraBoldFontWeight: '800',
  ultraBoldFontWeight: '900',

  fontFamily: '',
  fontFamilyBold: '',

  secondaryFontFamily: '',
  secondaryFontFamilyBold: '',
};

export const darkTheme = {
  bgColor: '#1b1b1b',
  bgColorLight: '#222323',
  bgColorDark: '#1a1a1a',
  bgColorVeryDark: '#171717',

  bgColorSecondary: '#f5f5f5',
  bgColorSecondaryLight: '#ffffff',

  textColor: '#f6f6f6',
  textColorLight: '#f7f7f7',
};
export const lightTheme = {
  bgColor: '#f5f5f5',
  bgColorLight: '#ffffff',
  bgColorDark: '#eeeeee',
  bgColorVeryDark: '#dddddd',

  bgColorSecondary: '#1b1b1b',
  bgColorSecondaryLight: '#222323',

  textColor: '#111111',
  textColorLight: '#222222',
};

export const colors = {
  green: {
    colorFaint: '#E8F5E9',
    colorLight: '#81C784',
    color: '#4CAF50',
    colorIntense: '#2E7D32',
    colorDark: '#ffffff',
  },
  purple: {
    color: '#8F75A5',
  },
  lilac: {
    color: '#BBE7FE',
  },
};

export const getThemeColors = template => {
  const themeColors = template === 'dark' ? darkTheme : lightTheme;
  return {
    ...base,
    ...themeColors,
    ...colors,
  };
};
