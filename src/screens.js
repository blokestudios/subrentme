import React from 'react';
import {Navigation} from 'react-native-navigation';
import {ThemeProvider} from 'styled-components';

import Onboarding from './containers/Onboarding/Onboarding';
import Home from './containers/Home/Home';
import List from './containers/List/List';
import Activity from './containers/Activity/Activity';
import Messages from './containers/Messages/Messages';
import Settings from './containers/Settings/Settings';
import {getThemeColors} from './theme.js';

export function registerScreens(store, Provider) {
  const state = store.getState();
  const [theme] = state.settings.values.filter(set => set.key === 'template');
  const themeColors = getThemeColors(theme.value);
  Navigation.registerComponent(
    'subrentme.onboarding',
    () => props => (
      <Provider store={store}>
        <ThemeProvider theme={themeColors}>
          <Onboarding {...props} />
        </ThemeProvider>
      </Provider>
    ),
    () => Onboarding,
  );
  Navigation.registerComponent(
    'subrentme.home',
    () => props => (
      <Provider store={store}>
        <ThemeProvider theme={themeColors}>
          <Home {...props} />
        </ThemeProvider>
      </Provider>
    ),
    () => Home,
  );
  Navigation.registerComponent(
    'subrentme.activity',
    () => props => (
      <Provider store={store}>
        <ThemeProvider theme={themeColors}>
          <Activity {...props} />
        </ThemeProvider>
      </Provider>
    ),
    () => Activity,
  );
  Navigation.registerComponent(
    'subrentme.messages',
    () => props => (
      <Provider store={store}>
        <ThemeProvider theme={themeColors}>
          <Messages {...props} />
        </ThemeProvider>
      </Provider>
    ),
    () => Messages,
  );
  Navigation.registerComponent(
    'subrentme.list',
    () => props => (
      <Provider store={store}>
        <ThemeProvider theme={themeColors}>
          <List {...props} />
        </ThemeProvider>
      </Provider>
    ),
    () => List,
  );
  Navigation.registerComponent(
    'subrentme.settings',
    () => props => (
      <Provider store={store}>
        <ThemeProvider theme={themeColors}>
          <Settings {...props} />
        </ThemeProvider>
      </Provider>
    ),
    () => Settings,
  );
}
