const home = require('./home.png');
const saved = require('./heart.png');
const store = require('./store.png');
const search = require('./search.png');
const settings = require('./settings.png');
const homeSelected = require('./selected/home.png');
const savedSelected = require('./selected/heart.png');
const storeSelected = require('./selected/store.png');
const searchSelected = require('./selected/search.png');
const settingsSelected = require('./selected/settings.png');

export default {
  home,
  saved,
  store,
  search,
  settings,
  homeSelected,
  savedSelected,
  storeSelected,
  searchSelected,
  settingsSelected,
};
