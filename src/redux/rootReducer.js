import {combineReducers} from 'redux';
import onboarding from './modules/onboarding';
import auth from './modules/auth';
import settings from './modules/settings';
import theme from './modules/theme';

const rootReducer = combineReducers({
  onboarding,
  auth,
  settings,
  theme,
});

export default rootReducer;
