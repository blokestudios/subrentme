import {all} from 'redux-saga/effects';
// import {watchers as authWatchers} from '../modules/auth';
// import {watchers as onboardingWatchers} from '../modules/onboarding';
// import {watchers as settingsWatchers} from '../modules/settings';
import {watchers as themeWatchers} from '../modules/theme';

export default function* root() {
  yield all([...themeWatchers]);
}
