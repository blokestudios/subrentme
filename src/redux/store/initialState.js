import {getThemeColors} from '../../theme';

export default {
  onboarding: {},
  auth: {
    authStatus: 1,
  },
  settings: {
    values: [
      {
        key: 'template',
        value: 'light',
      },
    ],
  },
  theme: getThemeColors('light'),
};
