import {fork, takeEvery, select, put} from 'redux-saga/effects';
import initialState from '../store/initialState';
import {getThemeColors} from '../../theme';

const SETTINGS_UPDATE = 'subrentme/settings/SET_VALUE';
const UPDATE_THEME = 'subrentme/theme/UPDATE_THEME';

export default function(state = initialState, action = {}) {
  switch (action.type) {
    case UPDATE_THEME: {
      return {
        ...action.theme,
      };
    }
    default:
      return state;
  }
}

export function* updateTheme() {
  console.log('fuckl');
  const getSettingsState = state => state.settings.values;
  const getThemeState = state => state.theme;
  const [settings] = yield select(getSettingsState);
  const theme = yield select(getThemeState);
  const themeColor = getThemeColors(settings.value);

  if (themeColor !== theme) {
    yield put({type: UPDATE_THEME, theme: themeColor});
  }
}

function* watchSettingsUpdate() {
  yield takeEvery(SETTINGS_UPDATE, updateTheme);
}

export const watchers = [fork(watchSettingsUpdate)];
