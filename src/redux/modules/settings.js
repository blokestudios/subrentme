import initialState from '../store/initialState';
import {all, put} from 'redux-saga/effects';

const RESET_VALUES = 'subrentme/settings/RESET_VALUE';
const SET_VALUE = 'subrentme/settings/SET_VALUE';

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case RESET_VALUES: {
      return {
        ...state,
      };
    }
    case SET_VALUE: {
      return {
        ...state,
        values: [
          ...state.values.filter(setting => setting.key !== action.setting.key),
          action.setting,
        ],
      };
    }
    default:
      return state;
  }
}

export function setUIValue({key, value}) {
  console.log('got here');
  return {
    type: SET_VALUE,
    setting: {key, value},
  };
}

export function* setUIValues(values) {
  yield all([...values.map(settings => put(setUIValue(settings)))]);
}

export const setValue = ({key, value}) => setUIValue({key, value});

export function resetValue() {
  return {
    type: RESET_VALUES,
  };
}
